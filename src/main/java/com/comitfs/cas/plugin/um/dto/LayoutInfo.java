
package com.comitfs.cas.plugin.um.dto;

import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "layoutName",
    "layoutId",
    "headers"
})
@Generated("jsonschema2pojo")
public class LayoutInfo {

    @JsonProperty("layoutName")
    private String layoutName;
    @JsonProperty("layoutId")
    private String layoutId;
    @JsonProperty("headers")
    private List<Header> headers = null;

    @JsonProperty("layoutName")
    public String getLayoutName() {
        return layoutName;
    }

    @JsonProperty("layoutName")
    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }

    @JsonProperty("layoutId")
    public String getLayoutId() {
        return layoutId;
    }

    @JsonProperty("layoutId")
    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
	
    }

    @JsonProperty("headers")
    public List<Header> getHeaders() {
        return headers;
    }

    @JsonProperty("headers")
    public void setHeaders(List<Header> headers) {
        this.headers = headers;
    }

}
