package com.comitfs.cas.plugin.um.dto;

public class DatabaseSequence {

	private String _id;
	private int seq;
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	public DatabaseSequence(String _id, int seq) {
		this._id = _id;
		this.seq = seq;
	}
	public DatabaseSequence() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
}
