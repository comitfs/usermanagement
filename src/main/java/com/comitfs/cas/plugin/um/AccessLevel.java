package com.comitfs.cas.plugin.um;

import java.util.List;

public class AccessLevel {
    private int accessLevel;
    private String reportType;
    private List<String> actions;
    private List<Criterion> criteria;
    private List<String> reportLocale;
    private List<String> platform;
    private String app;
    private String description;
    private Layout layout;

    // Getters and Setters
    public int getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public List<String> getActions() {
        return actions;
    }

    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    public List<Criterion> getCriteria() {
        return criteria;
    }

    public void setCriteria(List<Criterion> criteria) {
        this.criteria = criteria;
    }

    public List<String> getReportLocale() {
        return reportLocale;
    }

    public void setReportLocale(List<String> reportLocale) {
        this.reportLocale = reportLocale;
    }

    public List<String> getPlatform() {
        return platform;
    }

    public void setPlatform(List<String> platform) {
        this.platform = platform;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Layout getLayout() {
        return layout;
    }

    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    // Inner classes for Criterion and Layout
    public static class Criterion {
        private String name;
        private String value;

        // Getters and Setters
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class Layout {
        private String layoutName;
        private String layoutId;

        // Getters and Setters
        public String getLayoutName() {
            return layoutName;
        }

        public void setLayoutName(String layoutName) {
            this.layoutName = layoutName;
        }

        public String getLayoutId() {
            return layoutId;
        }

        public void setLayoutId(String layoutId) {
            this.layoutId = layoutId;
        }
    }
}

