package com.comitfs.cas.plugin.um.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import com.mongodb.BasicDBObject;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;


public class MongoDBHandler {

    Logger log = LogManager.getLogger(getClass().getName());


    public MongoClientSettings getClientSettings(String uri) {
    	MongoClientSettings clientSettings = null;
    	try {
    		ConnectionString connectionString = new ConnectionString(uri);
    		CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
    		CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
    		 clientSettings = MongoClientSettings.builder()
    				.applyConnectionString(connectionString)
    				.codecRegistry(codecRegistry)
    				.build();
    	}catch(Exception e) {
    		log.error("Exception while creating MG client", e);
    	}
    	
        return clientSettings;
        
    }
    
    public void updateDocument(BasicDBObject basicDBObject, String collectionName) {
       
    }
    
    public void addDocument(BasicDBObject basicDBObject, String collectionName) {
        
    }
    
    public void deleteDocument(BasicDBObject basicDBObject, String collectionName) {
        
    }
    public void replaceDocument(BasicDBObject basicDBObject,String collectionName) {
    	
    }
    
    
   
}

