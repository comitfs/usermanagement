
package com.comitfs.cas.plugin.um.dto;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "accessViewDetails",
    "description",
    "displayName",
    "name",
    "type",
    "app"
})
@Generated("jsonschema2pojo")
public class Widget {

    @JsonProperty("accessViewDetails")
    private String accessViewDetails;
    @JsonProperty("description")
    private String description;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
    @JsonProperty("app")
    private String app;

    @JsonProperty("accessViewDetails")
    public String getAccessViewDetails() {
        return accessViewDetails;
    }

    @JsonProperty("accessViewDetails")
    public void setAccessViewDetails(String accessViewDetails) {
        this.accessViewDetails = accessViewDetails;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }
    @JsonProperty("app")
	public String getApp() {
		return app;
	}
    @JsonProperty("app")
	public void setApp(String app) {
		this.app = app;
	}
    


}
