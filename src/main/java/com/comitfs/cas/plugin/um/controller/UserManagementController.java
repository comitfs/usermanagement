package com.comitfs.cas.plugin.um.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.auth.AuthFactory;
import org.jivesoftware.openfire.auth.UnauthorizedException;

import com.comitfs.cas.plugin.um.dto.AccessLevel;
import com.comitfs.cas.plugin.um.dto.AccessLevelConfig;
import com.comitfs.cas.plugin.um.dto.HeaderVersion;
import com.comitfs.cas.plugin.um.dto.LayoutInfo;
import com.comitfs.cas.plugin.um.dto.ReportVersion;
import com.comitfs.cas.plugin.um.dto.UserProfile;
import com.comitfs.cas.plugin.um.dto.WidgetVersion;
import com.comitfs.cas.plugin.um.service.UserManagementServiceImpl;
import com.comitfs.cas.plugin.um.util.SecretService;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;

@Path("/api")
public class UserManagementController {

	Logger logger = LogManager.getLogger(Logger.class);

	@GET
	@Path("/authorize")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUserProfile( @HeaderParam("Authorization") String authString) {
		UserProfile userProfile = null;
		try {
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}

			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			String userName = getUserName(authString);
			userProfile = userManagementServiceImpl.getUserProfile(userName);
		}catch(Throwable e) {
			logger.error("Exception while fetching user Profile",e);
		}


		return Response.status(Response.Status.OK)
				.entity(userProfile).build();

	}

	@GET
	@Path("/accesslevel/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAccessLevelInfo( @PathParam("id") int accessLevelId , @HeaderParam("Authorization") String authString) {
		AccessLevel accessLevel = null;
		try {

			if(accessLevelId == 0 ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			accessLevel = userManagementServiceImpl.getAccessLevel(accessLevelId);
		}catch(Throwable e) {
			logger.error("Exception while getAccessLevelInfo", e);
		}
		return Response.status(Response.Status.OK)
				.entity(accessLevel).build();

	}
	
	@GET
	@Path("/widgets/{version}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getWidget( @PathParam("version") String version , @HeaderParam("Authorization") String authString) {
		WidgetVersion  widget = null;
		try {

			if(version ==null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			widget = userManagementServiceImpl.getWidgetVersion(version);
		}catch(Throwable e) {
			logger.error("Exception while getWidget", e);
		}
		return Response.status(Response.Status.OK)
				.entity(widget).build();

	}
	
	@GET
	@Path("/headers/{version}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getHeaders( @PathParam("version") String version , @HeaderParam("Authorization") String authString) {
		HeaderVersion  widget = null;
		try {

			if(version ==null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			widget = userManagementServiceImpl.getHeaderVersion(version);
		}catch(Throwable e) {
			logger.error("Exception while getWidget", e);
		}
		return Response.status(Response.Status.OK)
				.entity(widget).build();

	}
	
	@GET
	@Path("/subtabs/{version}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getReport( @PathParam("version") String version , @HeaderParam("Authorization") String authString) {
		ReportVersion  report = null;
		try {

			if(version == null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			report = userManagementServiceImpl.getReportVersion(version);
		}catch(Throwable e) {
			logger.error("Exception while getWidget", e);
		}
		return Response.status(Response.Status.OK)
				.entity(report).build();

	}
	
	@GET
	@Path("/config")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getConfig(  @HeaderParam("Authorization") String authString) {
		AccessLevelConfig  config = null;
		try {

			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			config = userManagementServiceImpl.getConfig();
		}catch(Throwable e) {
			logger.error("Exception while getWidget", e);
		}
		return Response.status(Response.Status.OK)
				.entity(config).build();

	}
	@GET
	@Path("/layout/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getLayout( @PathParam("id") String layoutId ,@QueryParam("version") String version, @HeaderParam("Authorization") String authString) {
		LayoutInfo layoutInfo = null;
		try {
			if(layoutId == null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			layoutInfo = userManagementServiceImpl.getLayoutInfo(layoutId,version);

		}catch(Throwable e) {
			logger.error("Exception while getLayout", e);
		}
		return Response.status(Response.Status.OK)
				.entity(layoutInfo).build();

	}

	@GET
	@Path("/alllayout")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllLayout( @HeaderParam("Authorization") String authString) {
		List<LayoutInfo> layout = null;
		try {
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			layout = userManagementServiceImpl.getAllLayoutInfo();

		}catch(Throwable e) {
			logger.error("Exception while getLayout", e);
		}
		return Response.status(Response.Status.OK)
				.entity(layout).build();

	}
	@GET
	@Path("/allaccesslevel")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllAccesslevel( @HeaderParam("Authorization") String authString) {
		List<AccessLevel> access = null;
		try {
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			access = userManagementServiceImpl.getAllAccessLevel();

		}catch(Throwable e) {
			logger.error("Exception while getLayout", e);
		}
		return Response.status(Response.Status.OK)
				.entity(access).build();

	}
	@GET
	@Path("/alluser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllUser( @HeaderParam("Authorization") String authString) {
		List<UserProfile> user = null;
		try {
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			user = userManagementServiceImpl.getAllUser();

		}catch(Throwable e) {
			logger.error("Exception while getLayout", e);
		}
		return Response.status(Response.Status.OK)
				.entity(user).build();

	}
	
	@GET
	@Path("/alldynamicreports/{username}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllDynamicReports( @HeaderParam("Authorization") String authString, @PathParam("username") String username) {
		List<Document> dynamicReport = null;
		try {
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			dynamicReport = userManagementServiceImpl.getAllDynamicReprot(username);

		}catch(Throwable e) {
			logger.error("Exception while getAllDynamicReports", e);
		}
		return Response.status(Response.Status.OK)
				.entity(dynamicReport).build();

	}
	
	
	
	@POST
	@Path("/accesslevel/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUpdateAccessLevelInfo(@PathParam("query") String query, AccessLevel accessLevel , @HeaderParam("Authorization") String authString) {
		try {

			if(accessLevel == null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			
			switch (query) {
			case "Add":
				userManagementServiceImpl.addAccessLevel(accessLevel);
				break;
			case "Update":
				userManagementServiceImpl.updateAccessLevel(accessLevel);
				break;
			case "Replace":
				userManagementServiceImpl.replaceAccessLevel(accessLevel);
				break;
			case "Delete":
				userManagementServiceImpl.deleteAccessLevel(accessLevel.getAccessLevel());
				break;
			default:
				break;
			}
		}catch(Throwable e) {
			logger.error("Exception while getAccessLevelInfo", e);
		}
		return Response.status(Response.Status.OK)
				.entity(accessLevel).build();

	}
	
	@POST
	@Path("/dynamicreport/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUpdateDynamicReportInfo(@PathParam("query") String query, String accessLevel , @HeaderParam("Authorization") String authString) {
		try {

			if(accessLevel == null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			
			switch (query) {
			case "Add":
				userManagementServiceImpl.addDynamicReport(accessLevel);
				break;

//			case "Delete":
//				userManagementServiceImpl.deleteAccessLevel(accessLevel.getAccessLevel());
//				break;
			default:
				break;
			}
		}catch(Throwable e) {
			logger.error("Exception while getAccessLevelInfo", e);
		}
		return Response.status(Response.Status.OK)
				.entity(accessLevel).build();

	}

	@POST
	@Path("/layout/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUpdateLayoutInfo(@PathParam("query") String query, LayoutInfo layoutInfo , @HeaderParam("Authorization") String authString) {
		try {

			if(layoutInfo == null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			switch (query) {
			case "Add":
				userManagementServiceImpl.addLayoutInfo(layoutInfo);
				break;
			case "Update":
				userManagementServiceImpl.updateLayoutInfo(layoutInfo);
				break;
			case "Replace":
				userManagementServiceImpl.replaceLayoutInfo(layoutInfo);
				break;
			case "Delete":
				userManagementServiceImpl.deleteLayoutInfo(layoutInfo.getLayoutId());
				break;
			default:
				break;
			}
		}catch(Throwable e) {
			logger.error("Exception while getAccessLevelInfo", e);
		}
		return Response.status(Response.Status.OK)
				.entity(layoutInfo).build();

	}
	
	@POST
	@Path("/userprofile/{query}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addUpdateUserProfile(@PathParam("query") String query, UserProfile userProfile , @HeaderParam("Authorization") String authString) {
		try {

			if(userProfile == null ){
				return Response.status(Response.Status.BAD_REQUEST)
						.entity("").build();
			}
			if(authString == null || !isUserAuthenticated(authString)){
				return Response.status(Response.Status.UNAUTHORIZED)
						.entity("").build();
			}
			UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
			switch (query) {
			case "Add":
				userManagementServiceImpl.addUserProfile(userProfile);
				break;
			case "Update":
				userManagementServiceImpl.updateUserProfile(userProfile);
				break;
			case "Delete":
				userManagementServiceImpl.deleteUserProfile(userProfile.getUserName());
				break;
			default:
				break;
			}
		}catch(Throwable e) {
			logger.error("Exception while getAccessLevelInfo", e);
		}
		return Response.status(Response.Status.OK)
				.entity(userProfile).build();

	}
	
	@POST
	@Path("/checknumber")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getNumber() {
		 logger.info("API has received a post request");
		 return Response.status(Response.Status.OK)
					.entity("").build();
	}

	

	private String getUserName(String authString) {
		String decodedAuth = "";
		String[] authParts = authString.split("\\s+");
		String authInfo = authParts[1];
		byte[] bytes = null;
		String userName = null;
		try {
			bytes = DatatypeConverter.parseBase64Binary(authInfo);
			decodedAuth = new String(bytes);
			if(decodedAuth != null && decodedAuth.contains(":")) {
				String [] authArray = decodedAuth.split(":");
				userName = authArray[0];
			}
			logger.debug(decodedAuth);
		} catch (Throwable e) {
			logger.error("Exception while getting username", e);
		}
		return userName;
	}

	private Boolean isUserAuthenticated(String authString){

		String decodedAuth = "";
		String[] authParts = authString.split("\\s+");
		String authInfo = authParts[1];
		Boolean isAuth = Boolean.TRUE;
		byte[] bytes = null;
		try {
			bytes = DatatypeConverter.parseBase64Binary(authInfo);
			decodedAuth = new String(bytes);
			if(decodedAuth != null && decodedAuth.contains(":")) {
				String [] authArray = decodedAuth.split(":");
				String userName = authArray[0];
				String password = authArray[1];
				AuthFactory.authenticate(userName, password);
			}
		} catch (UnauthorizedException e) {
			isAuth = Boolean.FALSE;
			logger.error("Exception while user Authenticating UnAuthorized", e);
		}catch (Throwable e) {
			logger.error("Exception while user Authenticating", e);
		}

		return isAuth;
	}
	
    @GET
    @Path("/getJwtToken")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getJwtToken( @HeaderParam("Authorization") String authString,@Context UriInfo uriInfo) {
           String token = null;
           try {
                  if(authString == null || !isUserAuthenticated(authString)){
                        return Response.status(Response.Status.UNAUTHORIZED)
                                      .entity("").build();
                  }

                  UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
                  String userName = getUserName(authString);
                  token = userManagementServiceImpl.generateToken(userName, uriInfo.getBaseUri().getHost());
           }catch(Throwable e) {
                  logger.error("Exception while getJwtToken", e);
           }


           return Response.status(Response.Status.OK)
                        .entity(token).build();

    }
    
    @GET
    @Path("/parseJwt/{jwt}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response parseJwt( @HeaderParam("Authorization") String authString,@Context UriInfo uriInfo,@PathParam("jwt") String jwt ) {
           Jws<Claims> jws = null;
           try {
                        jws = Jwts.parser()
                                      .requireIssuer(XMPPServer.getInstance().getServerInfo().getHostname())
                              .setSigningKeyResolver(SecretService.getInstance().getSigningKeyResolver())
                              .parseClaimsJws(jwt);
           }catch(JwtException e) {
                  logger.error("Exception while getJwtToken", e);
                  return Response.status(Response.Status.UNAUTHORIZED)
                               .entity("Token Expired").build();
           }catch(Throwable e) {
                  logger.error("Exception while getJwtToken", e);
           }


           return Response.status(Response.Status.OK)
                        .entity(jws).build();

    }

}



