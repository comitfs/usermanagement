package com.comitfs.cas.plugin.um;

import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.openfire.user.User;

import java.util.Collection;

public class OpenfireUserFetcher {

    private UserManager userManager;

    public OpenfireUserFetcher(UserManager userManager) {
        this.userManager = userManager;
    }

    public Collection<User> getAllUsers() {
        return userManager.getUsers();
    }
}

