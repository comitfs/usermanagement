package com.comitfs.cas.plugin.um.dto;

import java.util.List;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"type",
    "name",
    "displayName",
    "description",
    "accessViewDetails",
    "columns",
    
})
@Generated("jsonschema2pojo")
public class Widgets {
	
	@JsonProperty("type")
	private String type;
   
    @JsonProperty("name")
    private String name;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("accessViewDetails")
    private String accessViewDetails;
    @JsonProperty("columns")
	   private List<columns> columns= null;
    
    @JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	@JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("accessViewDetails")
    public String getAccessViewDetails() {
        return accessViewDetails;
    }

    @JsonProperty("accessViewDetails")
    public void setAccessViewDetails(String accessViewDetails) {
        this.accessViewDetails = accessViewDetails;
    }
    
    @JsonProperty("columns")
	public List<columns> getColumns() {
		return columns;
	}

	public void setColumns(List<columns> columns) {
		this.columns = columns;
	}
    
}
