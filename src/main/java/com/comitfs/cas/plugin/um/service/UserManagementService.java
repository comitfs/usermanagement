package com.comitfs.cas.plugin.um.service;

import java.util.List;

import org.bson.Document;

import com.comitfs.cas.plugin.um.dto.AccessLevel;
import com.comitfs.cas.plugin.um.dto.AccessLevelConfig;
import com.comitfs.cas.plugin.um.dto.Config;
import com.comitfs.cas.plugin.um.dto.HeaderVersion;
import com.comitfs.cas.plugin.um.dto.LayoutInfo;
import com.comitfs.cas.plugin.um.dto.Reports;
import com.comitfs.cas.plugin.um.dto.ReportVersion;
import com.comitfs.cas.plugin.um.dto.UserProfile;
import com.comitfs.cas.plugin.um.dto.WidgetVersion;
import com.comitfs.cas.plugin.um.dto.Widgets;
import com.mongodb.client.FindIterable;

public interface UserManagementService {
	public UserProfile getUserProfile(String userName);
	public AccessLevel getAccessLevel(int accessLevelId);
	public LayoutInfo getLayoutInfo(String layoutId,String version);
	public List<LayoutInfo> getAllLayoutInfo();
	public List<AccessLevel> getAllAccessLevel();
	public List<UserProfile> getAllUser();
	public List<Document> getAllDynamicReprot(String userName);
	
	public UserProfile addUserProfile(UserProfile userProfile);
	public AccessLevel addAccessLevel(AccessLevel accessLevel);
	public LayoutInfo addLayoutInfo(LayoutInfo layoutInfo);
	public String addDynamicReport(String dynamicReport);
	
	
	
	public UserProfile updateUserProfile(UserProfile userProfile);
	public AccessLevel updateAccessLevel(AccessLevel accessLevel);
	public LayoutInfo updateLayoutInfo(LayoutInfo layoutInfo);
	
	public UserProfile deleteUserProfile(String userName);
	 public AccessLevel deleteAccessLevel(int accessLevelId);
	 public LayoutInfo deleteLayoutInfo(String layoutId);
	 
	public LayoutInfo replaceLayoutInfo(LayoutInfo layoutInfo);
	public AccessLevel replaceAccessLevel(AccessLevel accessLevel);
	public UserProfile replaceUserProfile(UserProfile userProfile);
	
	public WidgetVersion getWidgetVersion(String version);
	public WidgetVersion addWidget(WidgetVersion widget);
	
	public ReportVersion getReportVersion(String version);
	public ReportVersion addReport(ReportVersion report);
	
	public Config addConfig(Config config);
	public AccessLevelConfig getConfig();
	
	public HeaderVersion getHeaderVersion(String version);
	
	
	

	
	 
	
}
