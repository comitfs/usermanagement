package com.comitfs.cas.plugin.um.service;

import static com.mongodb.client.model.Filters.eq;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.bson.Document;
import org.jivesoftware.openfire.XMPPServer;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.util.JiveGlobals;

import com.comitfs.cas.plugin.um.dao.MongoDBHandler;
import com.comitfs.cas.plugin.um.dto.AccessLevel;
import com.comitfs.cas.plugin.um.dto.AccessLevelConfig;
import com.comitfs.cas.plugin.um.dto.Config;
import com.comitfs.cas.plugin.um.dto.Criterium;
import com.comitfs.cas.plugin.um.dto.DatabaseSequence;
import com.comitfs.cas.plugin.um.dto.HeaderVersion;
import com.comitfs.cas.plugin.um.dto.LayoutInfo;
import com.comitfs.cas.plugin.um.dto.ReportVersion;
import com.comitfs.cas.plugin.um.dto.UserProfile;
import com.comitfs.cas.plugin.um.dto.WidgetVersion;
import com.comitfs.cas.plugin.um.util.SecretService;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.model.Updates;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class UserManagementServiceImpl implements UserManagementService{

	Logger log = LogManager.getLogger(Logger.class);

	private String mongoDBHost = JiveGlobals.getProperty("um.plugin.mongo.db.hostname", "localhost");
	private String mongoDBPort = JiveGlobals.getProperty("um.plugin.mongo.db.port", "27017");

	@Override
	public UserProfile getUserProfile(String userName) {
		UserProfile userProfile = null;
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<UserProfile> mongoCollection =  mongoDatabase.getCollection("UserProfile", UserProfile.class);
				userProfile = mongoCollection.find(eq("userName", userName)).first();
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return userProfile;
	}
	@Override
	public WidgetVersion getWidgetVersion(String version) {

		WidgetVersion widget = null;
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<WidgetVersion> mongoCollection =  mongoDatabase.getCollection("Widget", WidgetVersion.class);
				widget = mongoCollection.find(eq("version", version)).first();
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Widget ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return widget;
	}

	@Override
	public ReportVersion getReportVersion(String version) {
		ReportVersion report = null;
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<ReportVersion> mongoCollection =  mongoDatabase.getCollection("Report", ReportVersion.class);
				report = mongoCollection.find(eq("version", version)).first();
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Widget ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return report;
	}

	@Override
	public AccessLevelConfig getConfig() {
		AccessLevelConfig accessLevelConfig=new AccessLevelConfig();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<Config> mongoCollection =  mongoDatabase.getCollection("AccessLevelConfig", Config.class);
				FindIterable<Config> findIterable = mongoCollection.find();
				Iterator<Config> i= findIterable.iterator();

				while(i.hasNext()) {
					accessLevelConfig.getConfig().add(i.next());
				}
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Widget ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return accessLevelConfig;
	}

	@Override
	public AccessLevel getAccessLevel(int accessLevelId) {
		AccessLevel accessLevel = null;
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<AccessLevel> mongoCollection =  mongoDatabase.getCollection("AccessLevel", AccessLevel.class);
				accessLevel = mongoCollection.find(eq("accessLevel", accessLevelId)).first();
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return accessLevel;
	}

	@Override
	public LayoutInfo getLayoutInfo(String layoutId,String version) {
		LayoutInfo layoutInfo = null;
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<LayoutInfo> mongoCollection =  mongoDatabase.getCollection("LayoutInfo", LayoutInfo.class);
				layoutInfo = mongoCollection.find(eq("layoutId", layoutId)).first();

//				WidgetVersion widget= this.getWidgetVersion(version);
//				final List<Widgets> widgets = widget.getWidgets();
//
//				ReportVersion report = this.getReportVersion(version);
//				List<Reports> reports= report.getReports();



//				for(Header header : layoutInfo.getHeaders()) {
//					if(!reports.stream().filter(rept -> ( (Reports) rept).getName().equals(header.getName())).findFirst().isPresent())
//					{
//						header.setRouteURI("error");
//					}
//					List<Widgets> headerWidgets = header.getgetWidgets();
//
//					if(headerWidgets != null && !headerWidgets.isEmpty()) {
//						for(Widgets headerwid : headerWidgets) {
//							if(!widgets.stream().filter(wid -> ((Widgets) wid).getName().equals(headerwid.getName())).findFirst().isPresent()) {
//
//							}
//						}
//					}
//
//
//				}
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return layoutInfo;
	}



	@Override
	public UserProfile deleteUserProfile(String userName) {
		UserProfile userprofile = new UserProfile();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<UserProfile> mongoCollection =  mongoDatabase.getCollection("UserProfile", UserProfile.class);

				mongoCollection.deleteOne(Filters.eq("userName", userName)); 
				System.out.println("Document deleted successfully...");

			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return userprofile;
	}

	@Override
	public AccessLevel deleteAccessLevel(int accessLevelId){
		AccessLevel accessLevel = new AccessLevel();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<AccessLevel> mongoCollection =  mongoDatabase.getCollection("AccessLevel", AccessLevel.class);

				mongoCollection.deleteOne(Filters.eq("accessLevel", accessLevelId)); 
				System.out.println("Document deleted successfully...");

			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return accessLevel;
	}

	@Override
	public LayoutInfo deleteLayoutInfo(String layoutId) {
		LayoutInfo layout = new LayoutInfo();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<LayoutInfo> mongoCollection =  mongoDatabase.getCollection("LayoutInfo", LayoutInfo.class);
				mongoCollection.deleteOne(Filters.eq("layoutId", layoutId)); 
				System.out.println("Document deleted successfully...");

			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return layout;
	}




	@Override
	public UserProfile addUserProfile(UserProfile userProfile) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<UserProfile> mongoCollection =  mongoDatabase.getCollection("UserProfile", UserProfile.class);
				ReplaceOptions opts = new ReplaceOptions().upsert(true);
				mongoCollection.replaceOne(eq("userName",userProfile.getUserName()),userProfile,opts);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return userProfile;
	}

	@Override
	public AccessLevel addAccessLevel(AccessLevel accessLevel) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<AccessLevel> mongoCollection =  mongoDatabase.getCollection("AccessLevel", AccessLevel.class);
				mongoCollection.insertOne(accessLevel);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return accessLevel;
	}

	@Override
	public WidgetVersion addWidget(WidgetVersion widget) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<WidgetVersion> mongoCollection =  mongoDatabase.getCollection("Widget", WidgetVersion.class);
				mongoCollection.insertOne(widget);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Widget ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return widget;
	}

	@Override
	public ReportVersion addReport(ReportVersion report) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<ReportVersion> mongoCollection =  mongoDatabase.getCollection("Report", ReportVersion.class);
				mongoCollection.insertOne(report);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Report ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return report;
	}
	@Override
	public Config addConfig(Config config) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<Config> mongoCollection =  mongoDatabase.getCollection("AccessLevelConfig", Config.class);
				mongoCollection.insertOne(config);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Report ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return config;
	}
	@Override
	public LayoutInfo addLayoutInfo(LayoutInfo layoutInfo) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<LayoutInfo> mongoCollection =  mongoDatabase.getCollection("LayoutInfo", LayoutInfo.class);
				layoutInfo.setLayoutId(String.valueOf(this.getNextSequence("layout_sequence")));
				mongoCollection.insertOne(layoutInfo);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Layout Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return layoutInfo;
	}


	@Override
	public UserProfile updateUserProfile(UserProfile userProfile) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<UserProfile> mongoCollection =  mongoDatabase.getCollection("UserProfile", UserProfile.class);

				mongoCollection.updateOne(Filters.eq("userName", userProfile.getUserName()), Updates.set("userName", userProfile.getUserName()));       

			}
		}catch(Throwable e) {
			log.error("Exception while retriving UserProfile Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return userProfile;
	}

	@Override
	public AccessLevel updateAccessLevel(AccessLevel accessLevel) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<AccessLevel> mongoCollection =  mongoDatabase.getCollection("AccessLevel", AccessLevel.class);
				//				mongoCollection.updateOne(Filters.eq("accessLevel", "accessLevel.getAccessLevel()"), Updates.set("accessLevel","accessLevel.getAccessLevel()"));       
				mongoCollection.updateOne(Filters.eq("accessLevel",accessLevel.getAccessLevel()), Updates.set("accessLevel",accessLevel.getAccessLevel()));
			}

		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return accessLevel;
	}


	@Override
	public LayoutInfo updateLayoutInfo(LayoutInfo layoutInfo) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<LayoutInfo> mongoCollection =  mongoDatabase.getCollection("LayoutInfo", LayoutInfo.class);

				mongoCollection.updateOne(Filters.eq("layoutId", layoutInfo.getLayoutId()), Updates.set("layoutName", layoutInfo.getLayoutName()));       
			}
		}catch(Throwable e) {
			log.error("Exception while retriving LayoutInfo ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return layoutInfo;
	}
	@Override
	public LayoutInfo replaceLayoutInfo(LayoutInfo layoutInfo) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<LayoutInfo> mongoCollection =  mongoDatabase.getCollection("LayoutInfo", LayoutInfo.class);
				ReplaceOptions opts = new ReplaceOptions().upsert(true);
				mongoCollection.replaceOne(eq("layoutId",layoutInfo.getLayoutId()),layoutInfo,opts);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving LayoutInfo ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return layoutInfo;
	}
	@Override
	public AccessLevel replaceAccessLevel(AccessLevel accessLevel) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<AccessLevel> mongoCollection =  mongoDatabase.getCollection("AccessLevel", AccessLevel.class);

				mongoCollection.replaceOne(Filters.eq("accessLevel",accessLevel.getAccessLevel()),accessLevel);

			}
		}
		catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return accessLevel;
	}

	@Override
	public UserProfile replaceUserProfile(UserProfile userProfile) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<UserProfile> mongoCollection =  mongoDatabase.getCollection("UserProfile", UserProfile.class);

				mongoCollection.replaceOne(Filters.eq("userName", userProfile.getUserName()), userProfile);       


			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return userProfile;
	}
	public List<LayoutInfo> getAllLayoutInfo() {

		List<LayoutInfo >layoutInfo = new ArrayList<>();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<LayoutInfo> mongoCollection =  mongoDatabase.getCollection("LayoutInfo", LayoutInfo.class);
				FindIterable<LayoutInfo> findIterable=mongoCollection.find();
				Iterator<LayoutInfo> i=findIterable.iterator();
				while(i.hasNext()) {
					layoutInfo.add(i.next());
				}

			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return layoutInfo;
	}
	@Override
	public List<AccessLevel> getAllAccessLevel() {
		List<AccessLevel > access = new ArrayList<>();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<AccessLevel> mongoCollection =  mongoDatabase.getCollection("AccessLevel", AccessLevel.class);
				FindIterable<AccessLevel> findIterable=mongoCollection.find();
				Iterator<AccessLevel> i=findIterable.iterator();
				while(i.hasNext()) {
					access.add(i.next());
				}

			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return access;
	}
	@Override
	public List<UserProfile> getAllUser() {
		List<UserProfile > user = new ArrayList<>();
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<UserProfile> mongoCollection =  mongoDatabase.getCollection("UserProfile", UserProfile.class);
				FindIterable<UserProfile> findIterable=mongoCollection.find();
				Iterator<UserProfile> i=findIterable.iterator();
				while(i.hasNext()) {
					user.add(i.next());
				}

			}

		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return user;
	}

	public void loadUsers() {
		try {
			List<UserProfile> userProfiles = this.getUserDetails();
			this.assignAccessRolesToUser(userProfiles);
			userProfiles.stream().forEach(userProfile->{
				this.addUserProfile(userProfile);
			});
		}catch(Throwable e) {
			log.error("Exception while loading users ",e);
		}
	}

	private List<UserProfile> getUserDetails() {
		List<UserProfile> userProfiles = new ArrayList<UserProfile>();
		try {

			String[] roleList = {"admin", "Trade Auditor", "Trade Analyst", "Floor Lead"};
			String[] locationList = {"EMEA", "APAC", "US", "EU"};
			Collection<User> users = XMPPServer.getInstance().getUserManager().getUsers();
			users.stream().forEach(user->{
				UserProfile userProfile = new UserProfile();
				userProfiles.add(userProfile);
				//userProfile.setEmail(user.getEmail());
				//userProfile.setUserName(user.getUsername());
				Random rnd = new Random();
				int number = rnd.nextInt(999999);
				userProfile.setContactNumber(String.valueOf(number));
				userProfile.setLocation(locationList[rnd.nextInt(locationList.length)]);
				userProfile.setRole(roleList[rnd.nextInt(roleList.length)]);
				userProfile.setDepartment("ENG");
			});
		}catch (Throwable e) {
			log.error("Exception while get user Profiles",e);
		}

		return userProfiles;

	}

	private void assignAccessRolesToUser(List<UserProfile> userProfiles) {
		try {
			List<AccessLevel> accessLevels = getAllAccessLevel();

			userProfiles.stream().forEach(user->{
				accessLevels.stream().forEach(accesslevel->{
					Optional<Criterium> matchingObject = accesslevel.getCriteria().stream().
							filter(p -> p.getName().equals("role")).
							findFirst();
					if(matchingObject.get().getValue().equalsIgnoreCase(user.getRole())) {
						user.setReportAccessLevel(String.valueOf(accesslevel.getAccessLevel()));
					}
				});

			});
		}catch (Throwable e) {
			log.error("Exception while get Access Roles Users",e);
		}

		return ;

	}

	public  int getNextSequence(String name){
		MongoClient mongoClient = null;
		DatabaseSequence obj = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<DatabaseSequence> mongoCollection = mongoDatabase.getCollection("DatabaseSequence",DatabaseSequence.class);
				Document increases = new Document();
				increases.append("seq", 1);
				Document document = new Document();
				document.append("$inc", increases);

				obj =  mongoCollection.findOneAndUpdate(eq("_id", name), document);
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		

		return obj.getSeq();
	}	
	public String generateToken(String userName, String audience) throws UnsupportedEncodingException {
		JwtBuilder builder = Jwts.builder();


		builder.setIssuedAt(new Date());
		builder.setExpiration(new Date(System.currentTimeMillis() + 60 * 1000));
		builder.setAudience(audience);
		builder.setSubject("CAS JWT token generator");
		builder.setIssuer(XMPPServer.getInstance().getServerInfo().getHostname());
		//builder.claim("clientName", userName);
		builder.signWith(SignatureAlgorithm.HS256, SecretService.getInstance().getHS256SecretBytes());

		return  builder.compact();
	}

	private void ensureType(String registeredClaim, Object value, Class expectedType) {
		boolean isCorrectType = expectedType.isInstance(value) || expectedType == Long.class && value instanceof Integer;

		if (!isCorrectType) {
			String msg = "Expected type: " + expectedType.getCanonicalName() + " for registered claim: '" + registeredClaim + "', but got value: " + value + " of type: " + value.getClass()
			.getCanonicalName();
			throw new JwtException(msg);
		}
	}
	@Override
	public HeaderVersion getHeaderVersion(String version) {
		HeaderVersion widget = null;
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<HeaderVersion> mongoCollection =  mongoDatabase.getCollection("Headers", HeaderVersion.class);
				widget = mongoCollection.find(eq("version", version)).first();
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Widget ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return widget;
	}
	@Override
	public String addDynamicReport(String dynamicReport) {
		MongoClient mongoClient = null;
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<Document> mongoCollection =  mongoDatabase.getCollection("DynamicReport");
				mongoCollection.insertOne(Document.parse(dynamicReport));
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Dynamic report ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return dynamicReport;
	}
	@Override
	public List<Document> getAllDynamicReprot(String userName) {
		MongoClient mongoClient = null;
		List<Document> reportsList = new ArrayList<Document>();
		try {
			MongoDBHandler mongoDBHandler = new MongoDBHandler();
			MongoClientSettings mongoClientSettings =  mongoDBHandler.getClientSettings("mongodb://"+mongoDBHost+":"+mongoDBPort);
			if(mongoClientSettings != null) {
				mongoClient = MongoClients.create(mongoClientSettings);
				MongoDatabase mongoDatabase = mongoClient.getDatabase("casdb");
				MongoCollection<Document> mongoCollection =  mongoDatabase.getCollection("DynamicReport");
				FindIterable<Document> responsedocs = mongoCollection.find(eq("userName", userName));
				
				for(Document doc : responsedocs) {
					reportsList.add(doc);
				}
			}
		}catch(Throwable e) {
			log.error("Exception while retriving Access Level Info ",e);
		}finally {
			if(mongoClient != null) {
				mongoClient.close();
			}
		}
		return reportsList;
	}


}
