
package com.comitfs.cas.plugin.um.dto;

import java.util.List;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"username",
    "name",
    "email",
    "contactNumber",
    "role",
    "department",
    "location",
    "buildingAndFloor",
    "reportAccessLevel",
    "dynamicReports"
})
@Generated("jsonschema2pojo")
public class UserProfile {
	 @JsonProperty("userName")
	    private String userName;
    @JsonProperty("name")
    private String name;
    @JsonProperty("email")
    private String email;
    @JsonProperty("contactNumber")
    private String contactNumber;
    @JsonProperty("role")
    private String role;
    @JsonProperty("department")
    private String department;
    @JsonProperty("location")
    private String location;
    @JsonProperty("buildingAndFloor")
    private String buildingAndFloor;
    @JsonProperty("reportAccessLevel")
    private String reportAccessLevel;
    @JsonProperty("dynamicReports")
    private List<Object> dynamicReports;
    

    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    @JsonProperty("userName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty("contactNumber")
    public String getContactNumber() {
        return contactNumber;
    }

    @JsonProperty("contactNumber")
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    @JsonProperty("role")
    public void setRole(String role) {
        this.role = role;
    }

    @JsonProperty("department")
    public String getDepartment() {
        return department;
    }

    @JsonProperty("department")
    public void setDepartment(String department) {
        this.department = department;
    }

    @JsonProperty("location")
    public String getLocation() {
        return location;
    }

    @JsonProperty("location")
    public void setLocation(String location) {
        this.location = location;
    }

    @JsonProperty("buildingAndFloor")
    public String getBuildingAndFloor() {
        return buildingAndFloor;
    }

    @JsonProperty("buildingAndFloor")
    public void setBuildingAndFloor(String buildingAndFloor) {
        this.buildingAndFloor = buildingAndFloor;
    }

    @JsonProperty("reportAccessLevel")
    public String getReportAccessLevel() {
        return reportAccessLevel;
    }

    @JsonProperty("reportAccessLevel")
    public void setReportAccessLevel(String reportAccessLevel) {
        this.reportAccessLevel = reportAccessLevel;
    }

    @JsonProperty("dynamicReports")
	public List<Object> getDynamicReports() {
		return dynamicReports;
	}

    @JsonProperty("dynamicReports")
	public void setDynamicReports(List<Object> dynamicReports) {
		this.dynamicReports = dynamicReports;
	}
    
    

}
