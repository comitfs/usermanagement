package com.comitfs.cas.plugin.um.dto;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import org.bson.codecs.pojo.annotations.BsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mongodb.client.FindIterable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    
    "config",

})
@Generated("jsonschema2pojo")
public class AccessLevelConfig {

	@BsonProperty(value = "config")
    @JsonProperty("config")
    private List<Config> config = null;

	public List<Config> getConfig() {
		if(config == null) {
			config = new ArrayList<>();
		}
		return config;
	}

	public void setConfig(List<Config> config) {
		this.config = config;
	}
	
}
