package com.comitfs.cas.plugin.um.util;


import java.util.List;

import com.comitfs.cas.plugin.um.dto.*;
import com.comitfs.cas.plugin.um.service.UserManagementServiceImpl;
import com.google.gson.Gson;

public class TestAPI {
	public static void main(String[] args) {
		UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
		System.out.println(userManagementServiceImpl.getNextSequence("layout_sequence"));
		LayoutInfo layoutInfo =userManagementServiceImpl.getLayoutInfo("66","1");
		System.out.println(layoutInfo.toString());
//		String layoutInfoString= "{\r\n" + 
//				"    \"layoutName\": \"userdd 1\",\r\n" + 
//				"    \"layoutId\": \"11\",\r\n" + 
//				"    \"headers\": [\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"CoreReport\",\r\n" + 
//				"            \"displayName\": \"Core Report\",\r\n" + 
//				"            \"description\": \"diaplay all widgets\",\r\n" + 
//				"            \"routeURI\": \"core-report\",\r\n" + 
//				"            \"widgets\": [\r\n" + 
//				"                {\r\n" + 
//				"                    \"name\": \"CFGTrend\",\r\n" + 
//				"                    \"displayName\": \"CFG Trend\",\r\n" + 
//				"                    \"description\": \"CFG call Graph\",\r\n" + 
//				"                    \"accessViewDetails\": \"false\"\r\n" + 
//				"                },\r\n" + 
//				"                {\r\n" + 
//				"                    \"name\": \"RFBTrend\",\r\n" + 
//				"                    \"displayName\": \"RFB Trend\",\r\n" + 
//				"                    \"description\": \"RFB call Graph\",\r\n" + 
//				"                    \"accessViewDetails\": \"false\"\r\n" + 
//				"                },\r\n" + 
//				"                {\r\n" + 
//				"                    \"name\": \"EODTrend\",\r\n" + 
//				"                    \"displayName\": \"EOD Trend\",\r\n" + 
//				"                    \"description\": \"EOD call Graph\",\r\n" + 
//				"                    \"accessViewDetails\": \"false\"\r\n" + 
//				"                }\r\n" + 
//				"            ]\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"RFBTrend\",\r\n" + 
//				"            \"displayName\": \"RFB Trend\",\r\n" + 
//				"            \"description\": \"Ready for business\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"EODTrend\",\r\n" + 
//				"            \"displayName\": \"EOD Trend\",\r\n" + 
//				"            \"description\": \"End of the day report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"CFGTrend\",\r\n" + 
//				"            \"displayName\": \"CFG Trend\",\r\n" + 
//				"            \"description\": \"CFG trend report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"LinesInventory\",\r\n" + 
//				"            \"displayName\": \"Lines Inventory\",\r\n" + 
//				"            \"description\": \"Lines Inventory report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"CallsReport\",\r\n" + 
//				"            \"displayName\": \"Calls Report\",\r\n" + 
//				"            \"description\": \"Calls report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"Reconciliation\",\r\n" + 
//				"            \"displayName\": \"Reconciliation\",\r\n" + 
//				"            \"description\": \"Reconciliation report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"OrphanedCalls\",\r\n" + 
//				"            \"displayName\": \"Orphaned Calls\",\r\n" + 
//				"            \"description\": \"Orphaned Calls\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"DynamicReport\",\r\n" + 
//				"            \"displayName\": \"Dynamic Report\",\r\n" + 
//				"            \"description\": \"Dynamic Report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"LineUtilizationReport\",\r\n" + 
//				"            \"displayName\": \"Line Utilization Report\",\r\n" + 
//				"            \"description\": \"Line Utilization Report\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"LineUnderUtilization\",\r\n" + 
//				"            \"displayName\": \"Line Under Utilization\",\r\n" + 
//				"            \"description\": \"Line Under Utilization\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"UsageReportByUser\",\r\n" + 
//				"            \"displayName\": \"Usage Report By User\",\r\n" + 
//				"            \"description\": \"Usage Report By User\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"UsageByLocation\",\r\n" + 
//				"            \"displayName\": \"Usage By Location\",\r\n" + 
//				"            \"description\": \"Usage By Location\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"Location\",\r\n" + 
//				"            \"displayName\": \"Location\",\r\n" + 
//				"            \"description\": \"Location\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"Connections\",\r\n" + 
//				"            \"displayName\": \"Connections\",\r\n" + 
//				"            \"description\": \"Connections\"\r\n" + 
//				"        },\r\n" + 
//				"        {\r\n" + 
//				"            \"name\": \"Templates\",\r\n" + 
//				"            \"displayName\": \"Templates\",\r\n" + 
//				"            \"description\": \"Templates\"\r\n" + 
//				"        }\r\n" + 
//				"    ]\r\n" + 
//				"}\r\n" + 
//				"";
//		
//		LayoutInfo layoutInfo = new Gson().fromJson(layoutInfoString, LayoutInfo.class);
//		layoutInfo.setLayoutName("teetetet");
//		userManagementServiceImpl.replaceLayoutInfo(layoutInfo);
//		UserProfile userprofile = userManagementServiceImpl.getUserProfile("u122");
//	        userprofile.setUserName("Nikitha");
//	        System.out.println(userprofile.getUserName());
//		UserProfile adduser=userManagementServiceImpl.addUserProfile(userprofile);
//		UserProfile updateuser=userManagementServiceImpl.updateUserProfile(userprofile);
		//UserProfile deleteuser=userManagementServiceImpl.deleteUserProfile("Nikitha");
////		LayoutInfo layout=userManagementServiceImpl.getAllLayoutInfo();
//List<LayoutInfo> layout=userManagementServiceImpl.getAllLayoutInfo();
//		
//		for(LayoutInfo lay:layout) {
//			System.out.println(lay.getLayoutId());
//		}
//		List <AccessLevel> access=userManagementServiceImpl.getAllAccessLevel();
//		for(AccessLevel acc:access) {
//			System.out.println(acc.getAccessLevel());
//		}
//		layout.setLayoutName("Test1...");
//		  LayoutInfo layout1=userManagementServiceImpl.getLayoutInfo("10","1.0");
//		  layout1.setLayoutName("userlayout");
	//	LayoutInfo layoutInfo=userManagementServiceImpl.getLayoutInfo("10");
//		LayoutInfo addlayout=userManagementServiceImpl.addLayoutInfo(layout1);
	//	LayoutInfo deletelayout=userManagementServiceImpl.deleteLayoutInfo("122");
//		LayoutInfo update=userManagementServiceImpl.updateLayoutInfo(layout1);
		
//		System.out.println(userManagementServiceImpl.getLayoutInfo("10"));
//		System.out.println(layout.getLayoutId());
//	WidgetVersion widget=userManagementServiceImpl.getWidgetVersion("1.0");
//System.out.println(widget.getVersion());
//	ReportVersion report =userManagementServiceImpl.getReportVersion("1.0");
//	System.out.println(report.getVersion());
//	ReportVersion report1=userManagementServiceImpl.addReport(report);
//	widget.setVersion((String) "1.1");
//	AccessLevelConfig config=userManagementServiceImpl.getConfig();
//	config.setName("Nikitha");
//	System.out.println(config.getConfig().size());
//        Config config=userManagementServiceImpl.getConfig("roles");
//config.setName("role");
//FindIterable<Config> config=userManagementServiceImpl.getAccessLevelConfig("roles");

//Config config1=userManagementServiceImpl.addConfig(config);
//	AccessLevelConfig config1=userManagementServiceImpl.addAccessLevelConfig(config);
//WidgetVersion widget1=userManagementServiceImpl.addWidget(widget);
//  	  AccessLevel accessLevel = userManagementServiceImpl.getAccessLevel(1);
//  	accessLevel.setAccessLevel(1122);
//  	  AccessLevel access=userManagementServiceImpl.addAccessLevel(accessLevel);
//  	  AccessLevel updateaccess=userManagementServiceImpl.updateAccessLevel(accessLevel);/
//  	  AccessLevel deleteaccess=userManagementServiceImpl.deleteAccessLevel(112);
//  	  AccessLevel replaceaccess =userManagementServiceImpl.replaceAccessLevel(accessLevel);
//		System.out.println(accessLevel.getAccessLevel());
	}
}
