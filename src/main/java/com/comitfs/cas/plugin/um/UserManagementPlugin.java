package com.comitfs.cas.plugin.um;

import com.comitfs.cas.plugin.um.dao.MongoDBHandler;
import com.comitfs.cas.plugin.um.dto.UserProfile;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

import org.apache.tomcat.InstanceManager;
import org.apache.tomcat.SimpleInstanceManager;
import org.bson.Document;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.plus.annotation.ContainerInitializer;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jivesoftware.openfire.admin.AdminManager;
import org.jivesoftware.openfire.user.User;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.http.HttpBindManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jivesoftware.openfire.user.UserManager;
import org.jivesoftware.util.JiveGlobals;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserManagementPlugin implements Plugin {
    private Logger logger = LogManager.getLogger(UserManagementPlugin.class);
    private static final String PLUGIN_NAME = "usermanagement";
    private File pluginDirectory = null;
    private String mongoDBHost = JiveGlobals.getProperty("um.plugin.mongo.db.hostname", "localhost");
	private String mongoDBPort = JiveGlobals.getProperty("um.plugin.mongo.db.port", "27017");
	private String accessLevelFilePath = JiveGlobals.getProperty("um.plugin.access.level.file.path", "C:\\Program Files\\Openfire\\conf\\AccessLevel.json");
    private String master66FilePath = JiveGlobals.getProperty("um.plugin.master66.file.path", "C:\\Program Files\\Openfire\\conf\\Master66.json");

    private MongoDBHandler mongoDBHandler;

    @Override
    public void initializePlugin(PluginManager pluginManager, File file) {
        logger.info("Initializing User Management Plugin ....");
        this.pluginDirectory = file;
        this.mongoDBHandler = new MongoDBHandler(); // Initialize MongoDBHandler
        createWebAppContext();

        // Start a new thread to insert master data
        new Thread(this::insertMasterData).start();

        // Start a new thread to pre-populate the UserProfile collection
        new Thread(this::populateUserProfiles).start();
    }

    @Override
    public void destroyPlugin() {
        // Cleanup resources if necessary
    }

    private void createWebAppContext() {
        try {
            logger.debug("createWebAppContext - Creating web service " + PLUGIN_NAME);

            WebAppContext context = new WebAppContext(null, pluginDirectory + "/classes/apps", "/" + PLUGIN_NAME);
            context.setClassLoader(this.getClass().getClassLoader());

            final List<ContainerInitializer> initializers = new ArrayList<>();
            initializers.add(new ContainerInitializer(new JettyJasperInitializer(), null));
            context.setAttribute("org.eclipse.jetty.containerInitializers", initializers);
            context.setAttribute(InstanceManager.class.getName(), new SimpleInstanceManager());
            context.setWelcomeFiles(new String[]{"openlink.html"});

            HttpBindManager.getInstance().addJettyHandler(context);

        } catch (Exception e) {
            logger.error("Error creating webapp context", e);
        }
    }

    private void insertMasterData() {
        try {
        	String connectionString = String.format("mongodb://%s:%s", mongoDBHost, mongoDBPort); // Local MongoDB connection string
            MongoClientSettings settings = mongoDBHandler.getClientSettings(connectionString);
            try (MongoClient mongoClient = MongoClients.create(settings)) {
                MongoDatabase database = mongoClient.getDatabase("userManagement");

                logger.info("Connected to database: userManagement");
                
                // Check and insert data into AccessLevel collection
                boolean accessLevelExists = checkCollectionExists(database, "AccessLevel");
                if (!accessLevelExists) {
                    insertDataIfNotExists(database, "AccessLevel", new File(accessLevelFilePath));
                } else {
                    logger.info("AccessLevel collection already exists.");
                }

                // Check and insert data into LayoutInfo collection
                boolean layoutInfoExists = checkCollectionExists(database, "LayoutInfo");
                if (!layoutInfoExists) {
                    insertDataIfNotExists(database, "LayoutInfo", new File(master66FilePath));
                } else {
                    logger.info("LayoutInfo collection already exists.");
                }

            }
        } catch (Exception e) {
            logger.error("Error inserting master data", e);
            e.printStackTrace();
        }
    }

    private boolean checkCollectionExists(MongoDatabase database, String collectionName) {
        for (String name : database.listCollectionNames()) {
            if (name.equals(collectionName)) {
                return true;
            }
        }
        return false;
    }

    private void insertDataIfNotExists(MongoDatabase database, String collectionName, File jsonFile) {
        try {
            MongoCollection<Document> collection = database.getCollection(collectionName);

            long count = collection.countDocuments();
            logger.info("Collection " + collectionName + " document count: " + count);
            System.out.println("Collection " + collectionName + " document count: " + count);

            if (count == 0) {
                String jsonContent = new String(Files.readAllBytes(jsonFile.toPath()));
                Document document = Document.parse(jsonContent);
                collection.insertOne(document);
                logger.info("Inserted data into collection: " + collectionName);
                System.out.println("Inserted data into collection: " + collectionName);
            } else {
                logger.info("Collection " + collectionName + " already contains data. Skipping insertion.");
                System.out.println("Collection " + collectionName + " already contains data. Skipping insertion.");
            }
        } catch (IOException e) {
            logger.error("Error reading JSON file for collection: " + collectionName, e);
            e.printStackTrace();
        }
    }
    

    private void populateUserProfiles() {
        try {
            // Retrieve users from Openfire
            UserManager userManager = UserManager.getInstance();
            AdminManager adminManager = AdminManager.getInstance();
            Collection<User> users = userManager.getUsers();
            // Connect to MongoDB
            String mongoURI = "mongodb://" + mongoDBHost + ":" + mongoDBPort;
            MongoClientSettings settings = mongoDBHandler.getClientSettings(mongoURI);
            try (MongoClient mongoClient = MongoClients.create(settings)) {
                MongoDatabase database = mongoClient.getDatabase("userManagement");
                MongoCollection<Document> collection = database.getCollection("UserProfile");
                
                // Read JSON file and map/convert to POJO
                ObjectMapper objectMapper = new ObjectMapper(); 
                AccessLevel accessLevel = objectMapper.readValue(new File(accessLevelFilePath), AccessLevel.class);

                for (User user : users) {
                    String username = user.getUsername();
                    
                    // Check if user already exists in MongoDB
                    Document query = new Document("userName", username);
                    Document existingUser = collection.find(query).first();
                    
                    if (existingUser == null) {
                        // Create UserProfile object
                    	
                    	UserProfile userProfile = new UserProfile();
                        userProfile.setUserName(username);
                        userProfile.setName(user.getName());
                        userProfile.setEmail(user.getEmail());
                        userProfile.setRole(adminManager.isUserAdmin(username, false) ? "admin" : "user");
                        userProfile.setReportAccessLevel(adminManager.isUserAdmin(username, false) ? String.valueOf(accessLevel.getAccessLevel()) : "");
                        
                        // Convert UserProfile to a Document
                        Document userProfileDocument = Document.parse(new ObjectMapper().writeValueAsString(userProfile));
                        
                        // Insert into MongoDB
                        collection.insertOne(userProfileDocument);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error populating user profiles", e);
            System.out.println("Error populating user profiles" + e);
        }
    }



//    private void createWebAppContext() {
//		try {			
//
////			ServletContextHandler contexts = new WebAppContext(null, pluginDirectory.getPath(),"/" + PLUGIN_NAME);
////			contexts.setClassLoader(this.getClass().getClassLoader());
////
////			try {
////
////				contexts.setWelcomeFiles(new String[] { "index.html" });
////
////
////			} catch (Exception e) {
////				logger.info("Error while initializing component for UM Plugin", e);
////			}
//
//
//			// Add web-app.
////			new Thread(() ->{
////				UserManagementServiceImpl userManagementServiceImpl = new UserManagementServiceImpl();
////				userManagementServiceImpl.loadUsers();
////			}).start();
//			
//			final List<ContainerInitializer> initializers = new ArrayList<ContainerInitializer>();
//			initializers.add( new ContainerInitializer( new JettyJasperInitializer(), null ) );
//
//			ServletContextHandler context = new WebAppContext(null, pluginDirectory.getPath(), "/" + pluginDirectory.getName() );
//			context.setAttribute( "org.eclipse.jetty.containerInitializers", initializers );
//			context.setAttribute( InstanceManager.class.getName(), new SimpleInstanceManager() );
//			context.setWelcomeFiles( new String[]{"index.html"} );
//
//			HttpBindManager.getInstance().addJettyHandler( context );
//
//
//		} catch (Exception e) {
//			logger.error("Error initializing webcontext for UM Plugin", e);
//		}
//    }
}
