
package com.comitfs.cas.plugin.um.dto;

import java.util.List;
import javax.annotation.Generated;

import org.bson.codecs.pojo.annotations.BsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "accessLevel",
    "reportType",
    "actions",
    "criteria",
    "reportLocale",
    "platform",
    "description",
    "layout",
    "app"
})
@Generated("jsonschema2pojo")
public class AccessLevel {
	
	@BsonProperty(value = "accessLevel")
    @JsonProperty("accessLevel")
    private int accessLevel;
	@BsonProperty(value = "reportType")
    @JsonProperty("reportType")
    private String reportType;
	@BsonProperty(value = "actions")
    @JsonProperty("actions")
    private List<String> actions = null;
	@BsonProperty(value = "criteria")
    @JsonProperty("criteria")
    private List<Criterium> criteria = null;
	@BsonProperty(value = "reportLocale")
    @JsonProperty("reportLocale")
    private List<String> reportLocale = null;
	@BsonProperty(value = "platform")
    @JsonProperty("platform")
    private List<String> platform = null;
	@BsonProperty(value = "description")
    @JsonProperty("description")
    private String description;
	@BsonProperty(value = "app")
    @JsonProperty("app")
    private String app;
	@BsonProperty(value = "layout")
    @JsonProperty("layout")
    private Layout layout;

    @JsonProperty("accessLevel")
    public int getAccessLevel() {
        return accessLevel;
    }

    @JsonProperty("accessLevel")
    public void setAccessLevel(int accessLevel) {
        this.accessLevel = accessLevel;
    }

    @JsonProperty("reportType")
    public String getReportType() {
        return reportType;
    }

    @JsonProperty("reportType")
    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    @JsonProperty("actions")
    public List<String> getActions() {
        return actions;
    }

    @JsonProperty("actions")
    public void setActions(List<String> actions) {
        this.actions = actions;
    }

    @JsonProperty("criteria")
    public List<Criterium> getCriteria() {
        return criteria;
    }

    @JsonProperty("criteria")
    public void setCriteria(List<Criterium> criteria) {
        this.criteria = criteria;
    }

    @JsonProperty("reportLocale")
    public List<String> getReportLocale() {
        return reportLocale;
    }

    @JsonProperty("reportLocale")
    public void setReportLocale(List<String> reportLocale) {
        this.reportLocale = reportLocale;
    }

    @JsonProperty("platform")
    public List<String> getPlatform() {
        return platform;
    }

    @JsonProperty("platform")
    public void setPlatform(List<String> platform) {
        this.platform = platform;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("layout")
    public Layout getLayout() {
        return layout;
    }

    @JsonProperty("layout")
    public void setLayout(Layout layout) {
        this.layout = layout;
    }

    @JsonProperty("app")
	public String getApp() {
		return app;
	}

    @JsonProperty("app")
	public void setApp(String app) {
		this.app = app;
	}
    
    
}
