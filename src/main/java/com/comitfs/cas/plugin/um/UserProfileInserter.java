package com.comitfs.cas.plugin.um;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;

import java.util.Collection;

import org.bson.Document;
import org.jivesoftware.openfire.user.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserProfileInserter {
    private Logger logger = LoggerFactory.getLogger(UserManagementPlugin.class);
    private MongoClient mongoClient;
    private MongoDatabase database;

    public UserProfileInserter(String uri, String dbName) {
        this.mongoClient = MongoClients.create(uri);
        this.database = mongoClient.getDatabase(dbName);
    }

    public void insertUserProfiles(Collection<User> users, boolean isAdmin) {
        MongoCollection<Document> userProfileCollection = database.getCollection("UserProfile");
        UserProfileCreator profileCreator = new UserProfileCreator();

        for (User user : users) {
            Document userProfile = profileCreator.createUserProfileDocument(user, isAdmin);
            
            long count = userProfileCollection.countDocuments(new Document("username", user.getUsername()));
            if (count == 0) {
                userProfileCollection.insertOne(userProfile);
                logger.info("Inserted user profile for: " + user.getUsername());
            } else {
            	logger.info("User profile already exists for: " + user.getUsername());
            }
        }
    }
}

