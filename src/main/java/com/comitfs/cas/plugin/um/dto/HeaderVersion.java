package com.comitfs.cas.plugin.um.dto;

import java.util.List;
import javax.annotation.Generated;

import org.bson.codecs.pojo.annotations.BsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"version",
    "headers",
    

})
@Generated("jsonschema2pojo")
public class HeaderVersion {
	 @JsonProperty("version")
	    private String version;
	
	 @JsonProperty("version")
	    public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}
	@BsonProperty(value = "headers")
    @JsonProperty("headers")
    private List<Header> headers= null;

	public List<Header> getHeaders() {
		return headers;
	}

	public void setHeaders(List<Header> headers) {
		this.headers = headers;
	}

	




	 
	  
}