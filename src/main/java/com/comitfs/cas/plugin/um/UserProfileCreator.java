package com.comitfs.cas.plugin.um;

import org.bson.Document;
import org.jivesoftware.openfire.user.User;

public class UserProfileCreator {

    public Document createUserProfileDocument(User user, boolean isAdmin) {
        Document userProfile = new Document();
        userProfile.put("username", user.getUsername());
        userProfile.put("email", user.getEmail());
        userProfile.put("fullName", user.getName());
        userProfile.put("isAdmin", isAdmin);
        userProfile.put("AccessRoleId", isAdmin ? "MasterAccessRoleId" : "");

        return userProfile;
    }
}

