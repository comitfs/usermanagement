package com.comitfs.cas.plugin.um.dto;

import java.util.List;

import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"type",
    "name",
    "displayName",
    "description",
    "routeURI",
    "widgets",
})
@Generated("jsonschema2pojo")
public class Reports {
	@JsonProperty("type")
	private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("description")
    private String description;
    @JsonProperty("routeURI")
    private String routeURI;
    @JsonProperty("widgets")
    private List<Widgets> widgets = null;
 
 @JsonProperty("type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
    
    

	@JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("routeURI")
    public String getRouteURI() {
        return routeURI;
    }

    @JsonProperty("routeURI")
    public void setRouteURI(String routeURI) {
        this.routeURI = routeURI;
    }

    @JsonProperty("widgets")
    public List<Widgets> getWidgets() {
        return widgets;
    }

    @JsonProperty("widgets")
    public void setWidgets(List<Widgets> widgets) {
        this.widgets = widgets;
    }
}
