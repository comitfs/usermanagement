
package com.comitfs.cas.plugin.um.dto;

import javax.annotation.Generated;

import org.bson.codecs.pojo.annotations.BsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "layoutName",
    "layoutId"
})
@Generated("jsonschema2pojo")
public class Layout {
	@BsonProperty(value = "layoutName")
    @JsonProperty("layoutName")
    private String layoutName;
	@BsonProperty(value = "layoutId")
    @JsonProperty("layoutId")
    private String layoutId;

    @JsonProperty("layoutName")
    public String getLayoutName() {
        return layoutName;
    }

    @JsonProperty("layoutName")
    public void setLayoutName(String layoutName) {
        this.layoutName = layoutName;
    }

    @JsonProperty("layoutId")
    public String getLayoutId() {
        return layoutId;
    }

    @JsonProperty("layoutId")
    public void setLayoutId(String layoutId) {
        this.layoutId = layoutId;
    }

}
