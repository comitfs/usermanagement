package com.comitfs.cas.plugin.um.dto;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Generated;

import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.conversions.Bson;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mongodb.Block;
import com.mongodb.CursorType;
import com.mongodb.Function;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Collation;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	
    "name",
    "values"
})
@Generated("jsonschema2pojo")
public class Config {
	@BsonProperty(value = "name")
    @JsonProperty("name")
    private String name;
	@BsonProperty(value = "values")
    @JsonProperty("values")
    private List<String> values;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("values")
    public List<String> getValues() {
        return values;
    }

    @JsonProperty("values")
    public void setValues(List<String> values) {
        this.values = values;
    }
}


	
	