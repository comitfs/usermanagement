package com.comitfs.cas.plugin.um.dto;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import org.bson.codecs.pojo.annotations.BsonProperty;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.mongodb.client.FindIterable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"version",
    "widgets",
    

})
@Generated("jsonschema2pojo")
public class WidgetVersion {
	 @JsonProperty("version")
	    private String version;
	
	 @JsonProperty("version")
	    public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}
	@BsonProperty(value = "widgets")
    @JsonProperty("widgets")
    private List<Widget> widgets= null;

	public List<Widget> getWidgets() {
		return widgets;
	}

	public void setWidgets(List<Widget> widgets) {
		this.widgets = widgets;
	}
	 
	  
}