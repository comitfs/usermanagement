
package com.comitfs.cas.plugin.um.dto;

import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "description",
    "displayName",
    "name",
    "routeURI",
    "type",
    "widgets",
    "summaryWidgets",
    "app"
})
@Generated("jsonschema2pojo")
public class Subtab {

    @JsonProperty("description")
    private String description;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("name")
    private String name;
    @JsonProperty("routeURI")
    private String routeURI;
    @JsonProperty("type")
    private String type;
    @JsonProperty("app")
    private String app;
    @JsonProperty("widgets")
    private List<Widget> widgets;
    @JsonProperty("summaryWidgets")
    private List<Widget> summaryWidgets;

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("routeURI")
    public String getRouteURI() {
        return routeURI;
    }

    @JsonProperty("routeURI")
    public void setRouteURI(String routeURI) {
        this.routeURI = routeURI;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("widgets")
    public List<Widget> getWidgets() {
        return widgets;
    }

    @JsonProperty("widgets")
    public void setWidgets(List<Widget> widgets) {
        this.widgets = widgets;
    }
    @JsonProperty("summaryWidgets")
	public List<Widget> getSummaryWidgets() {
		return summaryWidgets;
	}
    @JsonProperty("summaryWidgets")
	public void setSummaryWidgets(List<Widget> summaryWidgets) {
		this.summaryWidgets = summaryWidgets;
	}
    @JsonProperty("app")
	public String getApp() {
		return app;
	}
    @JsonProperty("app")
	public void setApp(String app) {
		this.app = app;
	}
    
    
}
